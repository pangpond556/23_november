<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
    	/*$news = News::find();
    	$this->view->setVars(["news" => $news]);*/
      $cat_active = 1;
      $cat1 = 1;
      $cat2 = 1;
      $cat3 = 1;
      $cat4 = 1;
      if($this->request->get("ac")){
        $cat_active = $this->request->get("ac");
      }
      if($this->request->get("ca1")){
        $cat1= $this->request->get("ca1");
      }
      if($this->request->get("ca2")){
        $cat2 = $this->request->get("ca2");
      }
      if($this->request->get("ca3")){
        $cat3 = $this->request->get("ca3");
      }
      if($this->request->get("ca4")){
        $cat4 = $this->request->get("ca4");
      }

    	$curl = curl_init();
    	curl_setopt($curl, CURLOPT_URL, "http://chromauniapp.ddns.net:3000/news");
    	curl_setopt($curl, CURLOPT_POST, false);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    	$result = curl_exec($curl);
    	curl_close($curl);
    	$result_arr = json_decode($result, true);
    	$this->view->setVars(["news" => $result_arr,
                            "ac"   => $cat_active,
                            "ca1"  => $cat1,
                            "ca2"  => $cat2,
                            "ca3"  => $cat3,
                            "ca4"  => $cat4]);
    }
    public function viewnewsAction()
    {
      if($this->request->isPost()) {
       $detail = $this->request->getPost("detail");
       $title = $this->request->getPost("title");
       $date = $this->request->getPost("date");
       $date_day = substr_replace($date," ",strpos($date,"T"));
       $date_time = substr($date ,strpos($date,"T")+1);
       $date__ = substr_replace($date_time," ",8);
       $date_ = $date_day." ".$date__;

       $this->view->setVars(["title" => $title,
                            "date" => $date_,
                            "detail" => $detail]);
      }
    }

}
