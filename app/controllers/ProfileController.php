<?php

class ProfileController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }

    public function timetableAction()
    {
      if($this->request->isPost()){
        $student_id = $this->request->getPost("student_id");
        $term = $this->request->getPost("term");
        $year = $this->request->getPost("year");
        $url = "https://www3.reg.cmu.ac.th/regist".$term.$year. "/public/result.php?id=". $student_id;
      	$this->view->setVars(["url" => $url]);
      }
    	// $this->assets->addJs("https://code.jquery.com/jquery-1.11.3.min.js");
    	// $this->assets->addJs("//cdnjs.cloudflare.com/ajax/libs/hogan.js/3.0.2/hogan.min.js");
    	// $this->assets->addJs("../public/src/templates/templates.js");
    	// $this->assets->addJs("../public/src/scheduler.js");
    	// $this->assets->addJs("../public/dist/scheduler.js");
    	// $this->assets->addCss("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css");
    	// $this->assets->addCss("../public/dist/scheduler.css");
    }
    public function inputtimetableAction()
    {

    }
    public function inputexamAction()
    {

    }
    public function ExamTableAction()
    {
      if($this->request->isPost()){
        $student_id = $this->request->getPost("student_id");
        $term = $this->request->getPost("term");
        $year = $this->request->getPost("year");
        $url = "https://www3.reg.cmu.ac.th/regist".$term.$year."/public/result.php?id=".$student_id;
        $urlex = "https://www3.reg.cmu.ac.th/regist".$term.$year."/exam/index.php?type=FINAL&term=".$term.$year;
        $this->view->setVars(["url" => $url]);
        $this->view->setVars(["urlex" => $urlex]);
      }
    }
}
