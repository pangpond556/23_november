$(document).ready(function(){
        var list = [
            {
                'name': 'Monday',
                'appointments': [<?php for($i=2;$i<$cout;$i++){if($arr_days[$i-2]== 'MTh' || $arr_days[$i-2] == 'Mo')
                {?>
                    {'start': '<?php echo $start_t[$i];?>', 'end': '<?php echo $end_t[$i];?>', 'title': '<?php echo $arr_s[$i-1];?>'},
                <?php  }}?>
                ]
            },
            {
                'name': 'Tuesday',
                'appointments': [<?php for($i=2;$i<$cout;$i++){if($arr_days[$i-2]== 'TuF' || $arr_days[$i-2] == 'Tu')
                {?>
                    {'start': '<?php echo $start_t[$i];?>', 'end': '<?php echo $end_t[$i];?>', 'title': '<?php echo $arr_s[$i-1];?>'},
                <?php  }}?>
                ]
            },
            {
                'name': 'Wednesday',
                'appointments': [<?php for($i=2;$i<$cout;$i++){if($arr_days[$i-2]== 'Wed' || $arr_days[$i-2] == 'We')
                {?>
                    {'start': '<?php echo $start_t[$i];?>', 'end': '<?php echo $end_t[$i];?>', 'title': '<?php echo $arr_s[$i-1];?>'},
                <?php  }}?>
                ]
            },
            {
                'name': 'Thursday',
                'appointments': [<?php for($i=2;$i<$cout;$i++){if($arr_days[$i-2]== 'MTh' || $arr_days[$i-2] == 'Thu')
                {?>
                    {'start': '<?php echo $start_t[$i];?>', 'end': '<?php echo $end_t[$i];?>', 'title': '<?php echo $arr_s[$i-1];?>'},
                <?php  }}?>
                ]
            }
            ,
            {
                'name': 'Friday',
                'appointments': [<?php for($i=2;$i<$cout;$i++){if($arr_days[$i-2]== 'TuF' || $arr_days[$i-2] == 'Fri')
                {?>
                    {'start': '<?php echo $start_t[$i];?>', 'end': '<?php echo $end_t[$i];?>', 'title': '<?php echo $arr_s[$i-1];?>'},
                <?php  }}?>
                ]
            }
        ];
        var steps = [
            '08:00',
            '08:30',
            '09:00',
            '09:30',
            '10:00',
            '10:30',
            '11:00',
            '11:30',
            '12:00',
            '12:30',
            '13:00',
            '13:30',
            '14:00',
            '14:30',
            '15:00',
            '15:30',
            '16:00',
            '16:30',
            '17:00',
            '17:30'
        ];

        var onClickAppointment = function(payload){
            $("#onClickAppointment").html(JSON.stringify(payload));
        };

        var $scheduler = $("#scheduler").schedulerjs({
            'list': list,
            'steps': steps,
            'pixelsPerHour': 110,
            'start': '09:30',
            'end': '10:30',
            'headName': 'Day',
            'onClickAppointment': onClickAppointment
           });
           // Change time after initialization
           $scheduler.schedulerjs('start', '11:00');
           $scheduler.schedulerjs('end', '11:30');
});